FROM node

# /app -> from the root directory, docker will create a folder named "App"
WORKDIR /app/node-app

# Copy everything from this current directory (local) to the /app directory in the container
COPY . /app

RUN npm install

EXPOSE 80

CMD ["node", "server.js"]